﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using testNavisaf.Models;

namespace testNavisaf.Controllers
{
    public class EmpleadoController : Controller
    {

        EmpleadosDBEntities db = new EmpleadosDBEntities();

        // GET: Empleado
        public ActionResult Index()
        {

            return View(db.tblEmpleados.ToList());
        }

        [HttpPost]
        public ActionResult BuscarXDocumento(string Documento)
        {
            var lista = db.tblEmpleados.Where(a => a.DocIdentidad.Contains(Documento));
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BuscarXNombres(string Nombres)
        {
            var lista = db.tblEmpleados.Where(a => a.Nombres.Contains(Nombres) || a.Apellidos.Contains(Nombres));
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Crear()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Crear(tblEmpleados empleado)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View();

                if(empleado.Sexo.ToUpper().Equals("M") && (empleado.Edad < 25 || empleado.Edad > 40))
                {
                    ModelState.AddModelError("", string.Format("El empleado {0} debe estar entre los 25 y los 40 años", empleado.Nombres));
                    return View();
                }else if (empleado.Sexo.ToUpper().Equals("F") && (empleado.Edad < 24 || empleado.Edad > 38))
                {
                    ModelState.AddModelError("", string.Format("El empleado {0} debe estar entre los 24 y los 38 años", empleado.Nombres));
                    return View();
                }

                using (db)
                {
                    db.tblEmpleados.Add(empleado);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Error al Guardar");
                return View();
            }
        }

        public ActionResult Editar(int id)
        {
            using (db)
            {
                return View(db.tblEmpleados.Find(id));
            }
        }


        [HttpPost]
        public ActionResult Editar(tblEmpleados empleado)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View();

                if (empleado.Sexo.ToUpper().Equals("M") && (empleado.Edad < 25 || empleado.Edad > 40))
                {
                    ModelState.AddModelError("", string.Format("El empleado {0} debe estar entre los 25 y los 40 años", empleado.Nombres));
                    return View();
                }
                else if (empleado.Sexo.ToUpper().Equals("F") && (empleado.Edad < 24 || empleado.Edad > 38))
                {
                    ModelState.AddModelError("", string.Format("El empleado {0} debe estar entre los 24 y los 38 años", empleado.Nombres));
                    return View();
                }

                using (db)
                {
                    tblEmpleados objEpleado = new tblEmpleados();
                    objEpleado = db.tblEmpleados.Find(empleado.Id);

                    objEpleado.DocIdentidad = empleado.DocIdentidad;
                    objEpleado.Nombres = empleado.Nombres;
                    objEpleado.Apellidos = empleado.Apellidos;
                    objEpleado.Edad = empleado.Edad;
                    objEpleado.NumHijos = empleado.NumHijos;
                    objEpleado.Direccion = empleado.Direccion;
                    objEpleado.Ciudad = empleado.Ciudad;
                    objEpleado.Telefono = empleado.Telefono;
                    objEpleado.Sexo = empleado.Sexo;
                    objEpleado.FechaNacimiento = empleado.FechaNacimiento;

                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Error al Guardar");
                return View();
            }
        }

        public ActionResult Eliminar(int id)
        {
            using (db)
            {
                var empleado = db.tblEmpleados.Find(id);
                db.tblEmpleados.Remove(empleado);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }

    }
}