Create Database EmpleadosDB;
Go

Use EmpleadosDB;
GO

Create Table tblEmpleados(
	Id Int Primary Key Identity,
	DocIdentidad Varchar(15),
	Nombres Varchar(250),
	Apellidos Varchar(250),
	Edad Int,
	NumHijos Int,
	Direccion Varchar(250),
	Ciudad Varchar(250),
	Telefono Varchar(20),
	Sexo Char(1),
	FechaNacimiento Datetime
);


--Drop table tblEmpleados